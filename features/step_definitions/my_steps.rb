Given(/^abro el juego$/) do
  visit '/'
end

Then(/^debo ver "(.*?)"$/) do |text|
	last_response.body.should =~ /#{text}/m
end

Then(/^debo ver un boton "(.*?)"$/) do |text|
	last_response.body.should =~ /<input type=\'submit\' id=\'#{text}\' value=\'#{text}\'/m
end

When(/^inicio el juego$/) do
  click_button('Jugar')
end

Then(/^debo ver alguna palabra$/) do 
  last_response.body.should =~ /______/m
end

Then(/^debo ver un campo de texto$/) do
    last_response.should have_xpath("//input[@type=\"text\"]")
end

When(/^introduzco la letra "(.*?)"$/) do |value|
  fill_in("letra", :with => value)
  click_button("Jugar")
end

Then(/^debe devolver un error$/) do
  last_response.body.should =~ /Fallaste/
end

Then(/^debe devolver un acierto$/) do
   last_response.body.should =~ /Acertaste/
end
