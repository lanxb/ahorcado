Feature:
	Como oficinista en el bagno
	Quiero abrir el juego en un navegador web
	Para poder jugar al ahorcado

Scenario:
	Given abro el juego
	Then debo ver "Bienvenido al juego del ahorcado"
	And debo ver un boton "Jugar"

Scenario:
	Given abro el juego
	When inicio el juego
	Then debo ver "a b c d e f g h i j k l m n o p q r s t u v w x y z"

