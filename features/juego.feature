Feature:
	Como oficinista en el bagno
	Quiero ver una palabra semi oculta
	Para iniciar el juego

Scenario:
	Given abro el juego
	When inicio el juego
	Then debo ver alguna palabra 

Scenario:
	Given abro el juego
	When inicio el juego
	Then debo ver un campo de texto

Scenario:
	Given abro el juego
	When inicio el juego 
	And introduzco la letra "z"
	Then debe devolver un error

Scenario:
	Given abro el juego
	When inicio el juego 
	And introduzco la letra "A"
	Then debe devolver un acierto
	And debo ver "_A____"
