require './lib/juego'

describe "Juego" do
	it "debe retornar las letras del alfabeto" do
		letras = Juego.letras_para_jugar
		letras.should == "a b c d e f g h i j k l m n o p q r s t u v w x y z"
	end
end