require './lib/diccionario'
describe "Diccionario" do

	before do 
		@game = Diccionario.new
	end


	it "debe retornar una palabra con parametros" do
		
		@game.iniciar 
		result = @game.get_Palabra 1

		result.should == "PATO"
	end

	it "debe de retornar false si la letra es T y la palabra es BEBER" do
		@game.iniciar 

		result = @game.verificar_palabra "BEBER","T"

		result.should == false
	end

	it "debe de retornar true si la letra es R y la palabra es BEBER" do
		@game.iniciar 

		result = @game.verificar_palabra "BEBER","R"

		result.should == true
	end

	it "debe de retornar true si la letra es R y la palabra es nula" do
		@game.iniciar 

		result = @game.verificar_palabra nil,"R"

		result.should == false
	end

	it "debe de retornar true si la letra es nil y la palabra es BEBER" do
		@game.iniciar 

		result = @game.verificar_palabra "BEBER",nil

		result.should == false
	end
 
	it "debe de retornar __T_ cuando recibe PATO como palabra de juego y T como letra al inicio del juego" do
		@game.iniciar 

		result = @game.enmascaraPalabra "PATO", "T", "____"

		result.should == "__T_"

	end

	it "debe de retornar P_T_ cuando recibe PATO como palabra de juego, P como jugada y __T_ como estado de juego" do
		@game.iniciar 

		result = @game.enmascaraPalabra "PATO", "P", "__T_"

		result.should == "P_T_"

	end

	it "debe retornar ______ (5 underscores) cuando recibe la palabra BEBER" do
		@game.iniciar 

		result = @game.convertir_palabra_a_guiones_bajos "BEBER"

		result.should == "_____"

	end

	it "debe retornar ERROR cuando recibe una palabra nula" do
		@game.iniciar 

		result = @game.convertir_palabra_a_guiones_bajos nil

		result.should == "ERROR"

	end
end