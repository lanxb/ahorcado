class Diccionario

	def iniciar
		@arrayPalabra = ['CAMINO','PATO','HOMBRE']
		@arrayPalabraRevisar = ""
	end

	def get_Palabra idPalabra

		
			@arrayPalabraRevisar = @arrayPalabra[idPalabra]
		

	end

	def verificar_palabra palabra,letra

		if palabra == nil || letra == nil
			result = false
		else
			result = palabra.include? letra
		end

		result
	end

	def enmascaraPalabra palabra, letra, estado
		arreglo_palabra = palabra.split("")
		arreglo_estado	= estado.split("")

		palabra_enmascarada = ''
		i=0
		arreglo_palabra.each do |l|
			if l == letra
				arreglo_estado[i] = letra
			end
			i+= 1
		end
		arreglo_estado.join("")
	end


	def convertir_palabra_a_guiones_bajos palabra
		if palabra == nil
			"ERROR"
		else
			'_' * palabra.size
		end
	end

end

