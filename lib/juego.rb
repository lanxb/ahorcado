class Juego

	def self.letras_para_jugar
		letras = Array.new
		'a'.upto 'z' do |l|
			letras.push l
		end
		letras.join " "
	end

	def self.letras_underscore palabra
		'A'.upto 'Z' do |l|
			palabra.tr l, "_ "
		end
		palabra
	end
end
