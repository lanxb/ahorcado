require 'sinatra'
require './lib/juego'
require './lib/diccionario'

class App 
	attr_accessor :palabra
	attr_accessor :palabra_juego
end

@@app = App.new

configure do
  enable :sessions
end

get '/' do
	erb :index
end

post '/juego' do
	diccionario = Diccionario.new
	diccionario.iniciar
	@letras = Juego.letras_para_jugar
	@@app.palabra = diccionario.get_Palabra 0 
 	@@app.palabra_juego = diccionario.convertir_palabra_a_guiones_bajos @@app.palabra
	erb :juego
end

post '/jugar_letra' do
	letra = params["letra"]
	@letras = Juego.letras_para_jugar
	diccionario = Diccionario.new
	resultado = diccionario.verificar_palabra @@app.palabra, letra.upcase 
	if !resultado 
		@mensaje = "Fallaste"	
	else 
		@mensaje = "Acertaste"

		@@app.palabra_juego = diccionario.enmascaraPalabra @@app.palabra, letra.upcase, @@app.palabra_juego
	end 

	erb :juego
end
